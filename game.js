var size = 6;
      var n = size * size;
      var left = n;
      var tile = new Array(n);
      var boardElements = new Array(n);
      var newGameButton, difficultyUpButton, difficultyDownButton;
      var oponNextMoveTime, playerNextMoveTime;
      var freezeGame;
      var playerScore = 0;
      var computerScore = 0;
      var startingDifficulty = 5;
      var currentDelay = 1150;
      var gorillasPosition = {
        white: 0,
        black: 0
      };
      var bonusMove = 0;
      var text1, text2, textDifficulty;
      var backgroundImage;


      var tilesScale = 0.4;
      var difficultyButtonScale = 0.3;
      var newgameButtonScale = 0.75;
      var spaceBetweenTiles = 15;
      var spaceBetweenInfo = 15;
      var spaceBetweenDifficultyButtons = 15;
      var fontSize = 16;
      var fontType = "" + fontSize + "px Arial";
      var popUpFontSize = 10;
      var popUpFontType = "" + popUpFontSize + "px Arial";
      var popUpFontFill = "#ff0000";
      var popUpFontAlign = "center";

      var tileWidth = 200;
      var newgameButtonHeight = 51;
      var difficultyButtonWidth = 111;
      var difficultyButtonHeight = 155;

      var oponentAI;

      function makeVirtualOponent(difficulty, delayed) {
        var board = new Array(n);
        for(var i = 0; i < n; i++) {
          board[i] = {
            status: 0,
            loss: 0
          };
        }
        var opon = {
          delayAmount: delayed,
          playerMove: true,
          boardMemory: board,
          difficultyLevel: difficulty,
          getIndex: function(x, y) {
            if(x < 1 || x > size || y < 1 || y > size) {
              return -1;
            }
            else {
              return (y - 1) * size + (x - 1);
            }
          },
          unknownNeighbors: function(x, notUsed) {
            //counts how many neighbors of x are not solved/remembered
            var num = 0;
            var index;
            for(var i = x % size; i <= x % size + 2; i++) {
              for(var j = Math.floor(x / size); j <= Math.floor(x / size) + 2; j++) {
                index = this.getIndex(i, j);
                if(index != -1 && index != notUsed) {
                  if(this.boardMemory[index].status != 1 && this.boardMemory[index].status != -1) {
                    num++;
                  }
                }
              }
            }
            return num;
          },
          chooseTileToCheck: function(x, y = -2) {
            //if not sure of postion
            if(this.boardMemory[x].status === 2) {
              var rand;
              //dont choose tile[x]
              var num = this.unknownNeighbors(x, y);
              rand = Math.floor(Math.random() * num) + 1;
              var i = x % size - 1;
              var j;
              var index;
              while(rand > 0 && i < x % size + 2) {
                i++;
                j = Math.floor(x / size) - 1;
                while(rand > 0 && j < Math.floor(x / size) + 2) {
                  j++;
                  index = this.getIndex(i, j);
                  if(index != -1) {
                    if(index != y && this.boardMemory[index].status != -1 && this.boardMemory[index].status != 1) {
                      rand--;
                    }
                  }
                }
              }
              return this.getIndex(i, j);
            }
            else {
              return x;
            }
          },
          memorize: function(x) {
            var r = Math.floor(Math.random() * 10);
            var i = 0;
            var memorized = false;
            if(r <= this.difficultyLevel * (2 / 3))
            while(i < n && !memorized) {
              if(x === tile[i]) {
                this.boardMemory[i].status = 1;
                this.boardMemory[i].loss = (5 - this.difficultyLevel) / 10;
                memorized = true;
                //testing
                console.log('Memorized ' + i + ' at status 1\n');
              }
              i++;
            }
            else {
              if(r <= this.difficultyLevel * (3 / 2))
              while(i < n && !memorized) {
                if(x === tile[i]) {
                  this.boardMemory[i].status = 2;
                  this.boardMemory[i].loss = (5 - this.difficultyLevel) / 10;
                  memorized = true;
                  //testing
                  console.log('Memorized ' + i + ' at status 2\n');
                }
                i++;
              }
            }
            //memory loss
            r = Math.random();
            for(var i = 0; i < n; i++) {
              if(this.boardMemory[i].status > 0) {
                if(this.boardMemory[i].loss >= r) {
                  this.boardMemory[i].status = (this.boardMemory[i].status + 1) % 3;
                  this.boardMemory[i].loss = (5 - this.difficultyLevel) / 10;
                  //testing
                  console.log('memory loss occured at ' + i + '\n' + 'now its status is ' + this.boardMemory[i].status + '\n');
                }
                else {
                  this.boardMemory[i].loss += (10 - this.difficultyLevel) / 50;
                }
              }
            }
          },
          generateTile: function() {
            var num = 0;
            for(var i = 0; i < n; i++) {
              if(this.boardMemory[i].status === 0) {
                num++;
              }
            }
            var r = game.rnd.integerInRange(1, num);
            var i = -1;
            while(r > 0 && (i + 1) < n) {
              i++;
              if(this.boardMemory[i].status === 0) r--;
            }
            return i;
          },
          makeYourMove: function() {
            var moved = false;
            var i = 0;
            var j = 1;
            // searching for memorized pair
            //searcing for a guarateed rmatch in memory
            while(i < n && !moved) {
              if(this.boardMemory[i].status === 1) {
                j = i + 1;
                while(j < n && !moved) {
                  if(this.boardMemory[j].status === 1 && boardElements[j] === boardElements[i]) {
                    //debug
                    if(j < 0 || i < 0 || j >= n || i >= n) {
                      window.alert('j = ' + j + '\ni = ' + i);
                    }
                    tileClicked(tile[i]);
                    tileClicked(tile[j]);
                    moved = true;
                  }
                  j++;
                }
              }
              i++;
            }
            //if not sure of a match then...
            i = 0;
            j = 1;
            while(i < n && !moved) {
              if(this.boardMemory[i].status > 0) {
                j = i + 1;
                while(j < n && !moved) {
                  if(this.boardMemory[j].status > 0 && boardElements[j] === boardElements[i]) {
                    var a = this.chooseTileToCheck(i);
                    var b = this.chooseTileToCheck(j, a);
                    //debug
                    if(b < 0 || a < 0 || b >= n || a >= n) {
                      window.alert('i = ' + i + '\na = ' + a + '\nj = ' + j + '\nb = ' + b);
                    }
                    tileClicked(tile[a]);
                    tileClicked(tile[b]);
                    moved = true;
                  }
                  j++;
                }
              }
              i++;
            }
            // moving otherwise
            if(!moved) {
              var r = this.generateTile();
              tileClicked(tile[r]);
              i = 0;
              // searching for a match in memory
              while(i < n && !moved) {
                if(i != r && this.boardMemory[i].status === 1 && boardElements[r] === boardElements[i]) {
                  //debug
                  if(i < 0 || i >= n) {
                    window.alert('i = ' + i + '\nr = ' + r);
                  }
                  tileClicked(tile[i]);
                  moved = true;
                }
                else if(i != r && this.boardMemory[i].status === 2 && boardElements[r] === boardElements[i]) {
                  var a = this.chooseTileToCheck(i, r);
                  //debug
                  if(i < 0 || a < 0 || i >= n || a >= n) {
                    window.alert('i = ' + i + '\na = ' + a + '\nr = ' + r);
                  }
                  tileClicked(tile[a]);
                  moved = true;
                }
                i++;
              }
              if(!moved) {
                var temp = this.boardMemory[r].status;
                this.boardMemory[r].status = 1;
                var r2 = this.generateTile();
                this.boardMemory[r].status = temp;
                tileClicked(tile[r2]);
              }
            }
          }
        }
        return opon;
      }

      //debugging function
      function dumpBoardMemory() {
        var string;
        for(var i = 0; i < size; i++) {
          string = '';
          for(var j = 0; j < size; j++) {
            string += (oponentAI.boardMemory[i * size + j].status + ' ');
          }
          console.log(string + '\n');
        }
      }

      //testing function
      function reverseBoard() {
        for(var i = 0; i < n; i++) {
          tile[i].tileBack.alpha =! tile[i].tileBack.alpha;
        }
      }

      var game = new Phaser.Game(750, 570, Phaser.AUTO, 'memory-game', {
        preload: preload,
        create: create,
        update: update
      }, true);

      function preload() {
        for(var i = 0; i < n / 2; i++)
          game.load.image('' + i, 'assets/' + i + '.png');

        game.load.image('back', 'assets/back_tile.png');
        game.load.image('button', 'assets/newgamebut.png');
        game.load.image('arrowDown', 'assets/strzaladol.png');
        game.load.image('arrowUp', 'assets/strzalagora.png');
        game.load.image('gorilla', 'assets/32.png');
        game.load.image('bg', 'assets/bg4.png');
          
      }

      function create() {
        // game.stage.backgroundColor = 0xffffff;
        // backgroundImage = game.add.image(0, 0, 'bg');
        // backgroundImage.scale.setTo(1.5, 1.5);
        //new game button
        newGameButton = game.add.button(size * (tileWidth * tilesScale + spaceBetweenTiles), 0, 'button', restartNewGame, this);
        newGameButton.scale.setTo(newgameButtonScale, newgameButtonScale);
        // results
        text1 = game.add.text(size * (tileWidth * tilesScale + spaceBetweenTiles), (newgameButtonHeight * newgameButtonScale + spaceBetweenInfo), "Player score: " + playerScore, {
          font: fontType
        });
        text2 = game.add.text(size * (tileWidth * tilesScale + spaceBetweenTiles), (newgameButtonHeight * newgameButtonScale + 2 * spaceBetweenInfo + fontSize), "Computer score: " + computerScore, {
          font: fontType
        });

        //difficulty
        textDifficulty = game.add.text(size * (tileWidth * tilesScale + spaceBetweenTiles), (newgameButtonHeight * newgameButtonScale + 3 * spaceBetweenInfo + 2 * fontSize), "Difficuly (0-10): " + startingDifficulty, {
          font: fontType
        });
        difficultyUpButton = game.add.button(size * (tileWidth * tilesScale + spaceBetweenTiles), (newgameButtonHeight * newgameButtonScale + 4 * spaceBetweenInfo + 3 * fontSize), 'arrowUp', difficultyUp, this);
        difficultyUpButton.scale.setTo(difficultyButtonScale, difficultyButtonScale);
        difficultyDownButton = game.add.button(size * (tileWidth * tilesScale + spaceBetweenTiles) + (difficultyButtonScale * difficultyButtonWidth + spaceBetweenDifficultyButtons), (newgameButtonHeight * newgameButtonScale + 4 * spaceBetweenInfo + 3 * fontSize), 'arrowDown', difficultyDown, this);
        difficultyDownButton.scale.setTo(difficultyButtonScale, difficultyButtonScale);

        createBoardGame(startingDifficulty);
      }

      function update() {
        if(!freezeGame) {
          if(left != 0) {
            game.input.enabled = oponentAI.playerMove;
          }
          else {
            game.input.enabled = true;
            if(playerScore > computerScore) {
              window.alert('Player wins!\nThe score is:\nPlayer: ' + playerScore + '\nComputer: ' + computerScore);
            }
            else if(playerScore < computerScore) {
              window.alert('Computer wins!\nThe score is:\nPlayer: ' + playerScore + '\nComputer: ' + computerScore);
            }
            else {
              window.alert('A draw!\nThe score is:\nPlayer: ' + playerScore + '\nComputer: ' + computerScore);
            }
            restartNewGame();
          }
          if(!oponentAI.playerMove && left > 0 && oponNextMoveTime <= game.time.now) {
            playerNextMoveTime = game.time.now  + oponentAI.delayAmount;
            oponNextMoveTime = game.time.now + oponentAI.delayAmount + 250;
            oponentAI.makeYourMove();
          }
        }
        else {
          game.input.enabled = false;
        }
      }

      function boardCheck() {
        for(var i = 0; i < n; i++) {
          if(oponentAI.boardMemory[i].status === -1) {
            tile[i].inputEnabled = false;
            tile[i].tileBack.alpha = false;
          }
          else {
            tile[i].inputEnabled = true;
            tile[i].tileBack.alpha = true;
          }
        }
      }

      function updateScoreBoard() {
        text1.text = "Player score: " + playerScore;
        text2.text = "Computer score: " + computerScore;
      }

      function updateDifficulty() {
        textDifficulty.text = "Difficuly (0-10): " + oponentAI.difficultyLevel;
      }

      function difficultyUp() {
        if(oponentAI.difficultyLevel < 10) {
          oponentAI.difficultyLevel++;
          updateDifficulty();
        }
      }

      function difficultyDown() {
        if(oponentAI.difficultyLevel > 0) {
          oponentAI.difficultyLevel--;
          updateDifficulty();
        }
      }

      function playerTileClicked(clickedTile) {
        if(game.time.now >= playerNextMoveTime && clickedTile.tileBack.alpha) {
          oponNextMoveTime = game.time.now + oponentAI.delayAmount;
          tileClicked(clickedTile);
        }
      }

      function tileClicked(clickedTile) {
        clickedTile.tileBack.alpha =! clickedTile.tileBack.alpha;
        oponentAI.memorize(clickedTile);
        checkForMatchingTiles();
      }

      function checkForMatchingTiles() {
        var x = -1;
        var y = -1;
        for(var i = 0; i < n; i++) {
          if(!tile[i].tileBack.alpha && tile[i].inputEnabled) {
            if(x === -1) x = i;
            else y = i;
          }
        }
        if(y != -1) {
          if(boardElements[x] === boardElements[y]) {
            actionCorrectMatch(x, y);
          }
          else {
            actionWrongMatch(x, y);
          }
        }
      }

      function actionCorrectMatch(x, y) {
        if(oponentAI.playerMove) {
          playerScore++;
          //if the gorillas are matched
          if(boardElements[x] === 0)
            playerScore += 2;
        }
        else {
          computerScore++;
          //if the gorillas are matched
          if(boardElements[x] === 0)
            computerScore += 2;
        }
        updateScoreBoard();
        // oponentAI.playerMove =! oponentAI.playerMove;
        //tiles remembered as solved
        tile[x].inputEnabled = false;
        tile[y].inputEnabled = false;

        oponentAI.boardMemory[x].status = -1;
        oponentAI.boardMemory[y].status = -1;

        left = left - 2;
        boardCheck();
      }

      function actionWrongMatch(x, y) {
        var player = oponentAI.playerMove;
        if(bonusMove === 0) {
          oponentAI.playerMove =! oponentAI.playerMove;
        }
        else {
          bonusMove--;
          // if(oponentAI.playerMove = false) {
          //   oponNextMove
          // }
        }
        var itsGorilla = -1;
        if(boardElements[x] === 0) {
          itsGorilla = x;
        }
        else if(boardElements[y] === 0) {
          itsGorilla = y;
        }
        if(itsGorilla != -1) {
          if(gorillasPosition.white === itsGorilla) {
            whiteGorilla(player, itsGorilla);
          }
          else {
            blackGorilla(player, x, y);
          }
        }
        else {
          game.time.events.add((oponentAI.delayAmount > 100 ? (oponentAI.delayAmount - 100) : (oponentAI.delayAmount / 2)), function() {
            tile[x].tileBack.alpha = true;
            tile[y].tileBack.alpha = true;
            boardCheck();
          }, this);
        }
      }

      function whiteGorilla(player, itsGorilla) {
        if(!player) {
          makePopUp('Your oponent\nwill see some tiles!');
          freezeGame = true;

          for(var i = 0; i < size; i++) {
            var temp = Math.floor(itsGorilla / size) * size + i;
            var rand = Math.floor(Math.random() * 3);
            if(rand === 0) {
              //debug
              console.log('Memorized from white gorilla:\n');
              oponentAI.memorize(temp);
            }
          }
          game.time.events.add(2000, function() {
            switchGorillas();
            boardCheck();
            freezeGame = false;
          })
        }
        else {
          makePopUp('Look fast!\nIt will disappear\nin a moment!');
          freezeGame = true;
          for(var i = 0; i < size; i++) {
            var temp = Math.floor(itsGorilla / size) * size + i;
            tile[temp].tileBack.alpha = false;
          }
          game.time.events.add(2000, function() {
            switchGorillas();
            boardCheck();
            freezeGame = false;
          })
        }
      }

      function blackGorilla(player, x, y) {
        if(!player) {
          makePopUp('Oponent loses his next 2 moves!');
        }
        else {
          makePopUp('Oh no!\nYou lose next 2 moves! :((')
        }
        bonusMove = 2;
        game.time.events.add((oponentAI.delayAmount > 100 ? (oponentAI.delayAmount - 100) : (oponentAI.delayAmount / 2)), function() {
          switchGorillas();
          tile[x].tileBack.alpha = true;
          tile[y].tileBack.alpha = true;
          boardCheck();
        }, this);
      }

      function switchGorillas() {
        var rand = Math.floor(Math.random() * 2);
        if(rand === 0) {
          var x = gorillasPosition.white;
          gorillasPosition.white = gorillasPosition.black;
          gorillasPosition.black = x;
          tile[gorillasPosition.white].loadTexture('gorilla');
          tile[gorillasPosition.black].loadTexture('0');
        }
      }31

      function makePopUp(string) {
          var popup = game.add.text(size * (tileWidth * tilesScale + spaceBetweenTiles), (newgameButtonHeight * newgameButtonScale + 5 * spaceBetweenInfo + 3 * fontSize + difficultyButtonScale * difficultyButtonHeight), string, {
            font: popUpFontType,
            fill: popUpFontFill,
            align: popUpFontAlign
          });
          game.time.events.add(1600, function() {
            popup.destroy();
          }, this);
      }

      function generateBoard(board_size) {
        var boardElements = new Array(board_size * board_size);
        var used = new Array(board_size * board_size / 2);
        var num_used = 0;
        var n = 0;
        var i, r;

        for(i = 0; i < board_size * board_size / 2; i++)
          used[i] = false;

        for(i = 0; i < board_size * board_size; i++)
          boardElements[i] = -1;

        while(n < board_size * board_size) {
          if(boardElements[n] === -1) {
            //find a random tile for boardElements[n]
            r = game.rnd.integerInRange(1, (board_size * board_size / 2 - num_used));
            i = -1;
            while(r > 0 && (i + 1) < board_size * board_size / 2) {
              i++;
              if(!used[i]) r--;
            }
            boardElements[n] = i;
            used[i] = true;
            num_used++;
            //add matching tile to the rest of the board
            r = game.rnd.integerInRange(1, (board_size * board_size - 2 * num_used + 1));
            i = n;
            while(r > 0 && (i + 1) < board_size * board_size) {
              i++;
              if(boardElements[i] === -1) r--;
            }
            boardElements[i] = boardElements[n];
          }
          n++;
        }

        return boardElements;
      }

      function createBoardGame(difficulty) {
        //generate board
        boardElements = generateBoard(size);
        for(var i = 0; i < n; i++) {

          tile[i] = game.add.image(i%size * (tileWidth * tilesScale + spaceBetweenTiles), Math.floor(i / size) * (tileWidth * tilesScale + spaceBetweenTiles), '' + boardElements[i]);
          tile[i].scale.setTo(tilesScale, tilesScale);
          tile[i].inputEnabled = true;
          tile[i].events.onInputDown.add(playerTileClicked, this);
          tile[i].tileBack = game.add.sprite(i%size * (tileWidth * tilesScale + spaceBetweenTiles), Math.floor(i / size) * (tileWidth * tilesScale + spaceBetweenTiles), 'back');
          tile[i].tileBack.scale.setTo(tilesScale, tilesScale);
        }
        //initialise oponent
        oponentAI = makeVirtualOponent(difficulty, currentDelay);
        oponNextMoveTime = game.time.now;

        playerNextMoveTime = game.time.now;
        freezeGame = false;

        //place gorillas
        var rand = Math.floor(Math.random() * 2);
        if(rand === 0) {
          var i = 0;
          while(i < n && boardElements[i] != 0) {
            i++;
          }
          tile[i].loadTexture('gorilla');
          gorillasPosition.white = i;
          i++;
          while(i < n && boardElements[i] != 0) {
            i++;
          }
          gorillasPosition.black = i;
        }
        else {
          var i = 0;
          while(i < n && boardElements[i] != 0) {
            i++;
          }
          gorillasPosition.black = i;
          i++;
          while(i < n && boardElements[i] != 0) {
            i++;
          }
          tile[i].loadTexture('gorilla');
          gorillasPosition.white = i;
        }

        //reset scoreboard
        playerScore = 0;
        computerScore = 0;
        left = n;
        updateScoreBoard();
      }

      function destroyBoardGame() {
        //clear all tiles
        for(var i = 0; i < n; i++) {
          tile[i].tileBack.destroy();
          tile[i].destroy();
        }
        //delete virtual oponent
        delete oponentAI;
      }

      function restartNewGame(buttonClicked) {
        var diffLevel = oponentAI.difficultyLevel;
        destroyBoardGame();
        createBoardGame(diffLevel);
      }